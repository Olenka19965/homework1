package scr;
import java.util.Random;
import java.util.Scanner;
public class Homework1 {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Let the game begin!");
        System.out.print("Enter your name; ");
        String name = scanner.nextLine();
        int randomNumber = random.nextInt(101);
        while (true) {
            System.out.print("Enter your guess: ");
            int guess = scanner.nextInt();
            if (guess < randomNumber) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (guess > randomNumber) {
                System.out.println("Your number is too big. Please try again.");
            } else {
                System.out.println("Congratulations, " + name + "!");
                break;
            }

        }
    }
}